package com.csv.controller;

import com.csv.message.ResponseMessage;
import com.csv.service.CsvFileService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Slf4j
@RestController
@RequestMapping("/api-v1/csv/upload")
public class CSVController {

  @Autowired
  private CsvFileService csvFileService;

  @PostMapping("/{color}")
  public ResponseEntity<ResponseMessage> uploadFile(@RequestParam("file") MultipartFile file, @PathVariable String color)
          throws IOException {
    log.info("[controller to upload data]  ");

    csvFileService.saveCsv(file, color);

    return ResponseEntity.status(HttpStatus.OK).body(
       new ResponseMessage("successfully uploaded file"));
  }
}
