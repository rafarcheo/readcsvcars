package com.csv.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "cars")
public class CsvCar {

    @Id
    @Column(name = "id")
    private long id;
    @Column(name = "nazwa")
    private String nazwa;
    @Column(name = "data_zakupu")
    private String data_zakupu;
    @Column(name = "kolor")
    private String  kolor;
}
