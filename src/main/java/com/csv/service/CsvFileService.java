package com.csv.service;

import com.csv.helper.CSVHelper;
import com.csv.model.CsvCar;
import com.csv.repository.CsvCarsRepository;
import com.sun.istack.internal.NotNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CsvFileService {

  @NotNull
  private CsvCarsRepository repository;

    /**
     * Handle save cars
     *
     * @param file csv file
     * @param color cars color to save
     * @throws IOException
     */
  public void saveCsv(MultipartFile file, String color) throws IOException {


      if (!CSVHelper.hasCSVFormat(file)) {
        throw new IOException("bad file format");
      }

      List<CsvCar> cars = CSVHelper.csvToCsvCars(file.getInputStream());

      saveCarsByColor(cars, color);
  }

    private void saveCarsByColor(List<CsvCar> cars, String color) {

      final List<CsvCar> carsFilteredByColor = cars.stream()
          .filter(car -> car.getKolor().equals(color))
          .collect(Collectors.toList());

      repository.saveAll(carsFilteredByColor);
    }
}
