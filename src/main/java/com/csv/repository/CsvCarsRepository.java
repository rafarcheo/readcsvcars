package com.csv.repository;

import com.csv.model.CsvCar;
import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;

@Repository
public interface CsvCarsRepository extends JpaRepository<CsvCar, Long> {
}
